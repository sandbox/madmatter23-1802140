<?php

/**
 * @file
 */

/**
 * Create a Solr Query, and return search results.
 *
 * @param string $search_string
 *   An solr formatted search query string
 * @param string $sort
 *   The sort by field. Default value is search_api_relevance
 * @param string $order
 *   Sortorder is set to DESC per default
 * @param string $search_fields
 *   Which fulltext fields to search in
 * @param string $return_fields
 *   Which fields to return. Default is complete node object.
 * @param string $facets
 *   Search in facets
 * @param string $page
 *   Which page to show
 * @param string $limit
 *   How many results to show per page.
 * @param string $address
 *
 * @param int $radius
 *
 *
 * @return array
 *   Search results
 */
function _services_search_api_callback($search_string, $sort, $order, $search_fields, $return_fields, $facets, $page, $limit, $address = NULL, $radius = NULL, $radius_units = NULL, $lat = NULL, $lng = NULL) {

  $fields = array();
  $unknown_fields = array();

  $active_search_index = variable_get('services_search_api_default_index', FALSE);

  if ($active_search_index === FALSE) {
    return services_error(t('Search returned no results.'), 404);
  }

  // Load the search index.
  $index = search_api_index_load($active_search_index);

  // Get a list of defined fields in the search index.
  $index_fields = $index->getFields();
  // Create a new search query.
  $query = $index->query();

  // Add the search string to the query.
  if ($search_string) {
    $query->keys($search_string);
  }
  // Map the fields we should search in.
  if ($search_fields) {
    $field_array = explode(',', $search_fields);
    foreach ($field_array as $field) {
      if (!array_key_exists(_map_solr_field($field), $index_fields)) {
        $unknown_fields[] = $field;
        continue;
      }
      $fields[] = _map_solr_field($field);
    }
    $query->fields($fields);
  }

  // Loop through defined facet groups.
  // And create search filters.
  if ($facets) {
    $facet_array = explode(';', trim($facets, ';'));
    foreach ($facet_array as $facet_info) {
      if (empty($facet_info)) {
        return services_error(t('Search returned no results. Due to an error in the search query.'), 404);
      }
      $facet_info_array = explode(':', $facet_info);

      // Check if a key value operator is defined in the search query.
      if (count($facet_info_array) < 3) {
        list($facet_name, $facet_value) = $facet_info_array;
        $operator = '=';
      }
      else {
        list($facet_name, $facet_value, $operator,
        ) = $facet_info_array;
      }

      if (!array_key_exists(_map_solr_field($facet_name), $index_fields)) {
        $unknown_fields[] = $facet_name;
        continue;
      }

      if (stristr($facet_value, ',')) {
        $facet_values = explode(',', $facet_value);
      }
      else {
        $facet_values = array($facet_value);
      }

      // Search conjunction internal between filters
      // set to OR.
      $filter = $query->createFilter('OR');

      // Set the filter conditions, default operator is =,
      // $facet_name = $facet_value.
      foreach ($facet_values as $facet_value) {
        $filter->condition(_map_solr_field($facet_name), $facet_value, $operator);
      }

      // Merge the filter into the query.
      $query->filter($filter);
    }
  }

  // Pass in spatial data.
  if (module_exists('search_api_location') && ($address || ($lat && $lng)) && $radius && $radius_units) {
    if ($locationfield = _search_api_location_get_locationfield($query->getIndex()->id)) {
      // Use lat & lng directly.
      if ($lat && $lng){
        $spatial['lat'] = $lat;
        $spatial['lng'] = $lng;
        $spatial['radius'] = $radius;
      }
      // Geocode address string.
      else {
        $params = array(
          'address' => $address,
          'radius' => $radius,
        );
        $spatial = _search_api_location_get_location(array(), $params);
      }
      $spatial['field'] = $locationfield;
      $spatial['radius_units'] = $radius_units;
      $query->setOption('spatial', $spatial);
    }
  }

  // Sort the filter by field and order.
  if ($sort && $order) {
    $query->sort(_map_solr_field($sort), $order);
  }

  // Set result range.
  $query->range($page * $limit, $limit);

  if (count($unknown_fields)) {
    watchdog('Search_api', 'These invalid fields have been found (%unknown_fields)', array('%unknown_fields' => implode(', ', $unknown_fields)), WATCHDOG_ERROR);
  }

  // Execute the search.
  try {
    $results = $query->execute();
  }
  catch(SearchApiException$e) {
    return services_error(t('Error: ' . $e->getMessage()), 404);
  }

  // Map the returned data correct, and return defined/default fields.
  if ($results && is_array($results) && count($results)) {
    $counter     = 0;
    $node        = array();
    $tmp_results = array();
    foreach ($results['results'] as $nid => $result) {

      // We check for the node here, because solr may have a record of a node
      // that no longer exists in Drupal.
      if ($node_object = node_load($nid)) {
        $wrapper = entity_metadata_wrapper('node', $node_object);
        // If return fields are specified, check if we have mapped them.
        if ($return_fields) {
          $fields = explode(',', $return_fields);
          foreach ($fields as $field) {

            $field_name = _map_solr_field($field);
            // The body field is a textarea, and we need to get data out,
            // in a different way.
            if ($field_name === 'body:value') {
              $field_name = 'body';
            }
            elseif ($field_name == 'field_product_image') {
              $fids = array();
              foreach ($wrapper->$field_name->value() as $images) {
                $fids[] = $images['fid'];
              }
              $node[$field] = supershop_file_get($fids);
            }
            if ($wrapper->$field_name->type() == 'text_formatted') {
              $node[$field] = $wrapper->$field_name->value->value();
            }
            elseif ($wrapper->$field_name->type() == 'user') {
              $node['author'] = array(
                'uid' => $wrapper->$field_name->value()->uid,
                'name' => $wrapper->$field_name->value()->name,
              );
            }
            else {
              if (isset($wrapper->$field_name->value()->rdf_mapping)) {
                unset($wrapper->$field_name->value()->rdf_mapping);
              }
              if ($field_name !== 'field_product_image') {
                $node[$field] = $wrapper->$field_name->value();
              }
            }
          }
        }
        $tmp = array();
        $tmp[$counter++] = new stdClass();
        $tmp[$counter]->score = $result['score'];
        $tmp[$counter]->id = $result['id'];
        $tmp[$counter] = array_merge((array) $tmp[$counter], $node);
        $tmp_results[] = $tmp[$counter];
      }
    }
    // If $limit is over 15, this is never fired.

    // Change the original results with new mapped fields.
    $results['results'] = $tmp_results;
    return $results;
  }

  // Return an 404 header if no result set is available.
  return services_error(t('Search returned no results.'), 404);
}

/**
 * Map fields named in the url, to fields in Apache Solr.
 *
 * @param string $field_name
 *   Field
 *
 * @return string
 *   Apache Solr field name.
 */
function _map_solr_field($field_name) {

  $field_map = array();

  // Get mapable fields from hook_map_solr_fields().
  foreach (module_implements('map_solr_fields') as $module_name) {
    $field_map = array_merge(module_invoke($module_name, 'map_solr_fields'), $field_map);
  }
  if ($field_name && !empty($field_map[$field_name])) {
    // Return the original solr field name.
    return $field_map[$field_name];
  }
  else {
    // If no map is found, we hope that the field is in the index.
    return $field_name;
  }
}
