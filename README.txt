Using the resources.
===============================================
HTTP Method: GET
Request URL: your_endpoint/search_api/retrieve


Standard GET Parameters:
-----------------------------------------------
  @param string $search_string
    Keywords.
  
  @param string $sort
    (optional) Defaults to search_api_relevance.
    
  @param string $order
    (optional) Defaults to DESC.

  @param string $search_fields
    (optional) Comma-separated list of fields to search. Defaults to title.

  @param string $return_fields
    (optional) Comma-separated list of fields to return. Defaults to title.
  
  @param string $facets
    (optional)
  
  @param int $limit
    (optional) Results per page. Defaults to 10.
    
  @param int $page
    (optional) Page number. Defaults to 0.


Location GET Parameters (available if search_api_location is enabled):
-----------------------------------------------
  @param string $address
    (optional) Address string to be geocoded by Google. See note #1.

  @param double $lat
    (optional) Latitude. See note #1.

  @param double $lng
    (optional) Longitude. See note #1.

  @param int $radius
    (optional) Geographic radius from geospatial point. Defaults to 5.

  @param string $radius_units
    (optional) Accepted values are mi (miles) or km (kilometers). Defaults to
    km.

Notes:
-----------------------------------------------
1. For geospatial searches, API will either:
   a) geocode the address parameter, or
   b) use the lat & lng parameters provided.
   If both are passed, lat & lng will take precedence.

2. There is a small chance that services will return fewer results that
   requested via $limit. This will only happen in the Search Index is outdated
   and attempts to return results for a node that has been recently deleted.

